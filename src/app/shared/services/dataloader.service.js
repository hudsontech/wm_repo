(function () {
    'use strict';

    angular.module('app').factory('DataLoader', [DataLoader]);

    /** @ngInject */
    function DataLoader() {
        return {
            getData: function () {
                return "data";
            }
        };
    }

})();