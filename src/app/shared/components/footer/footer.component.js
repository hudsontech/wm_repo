(function() {
    'use strict';
  
    angular.module('app').component('appFooter', {
      controller: FooterController,
      controllerAs: 'vm',
      templateUrl: 'app/shared/components/footer/footer.view.html',
    });
  
    /** @ngInject */
    function FooterController() {
    }
  
  })();
  