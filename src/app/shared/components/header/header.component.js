(function() {
    'use strict';
  
    angular.module('app').component('appHeader', {
      controller: HeaderController,
      controllerAs: 'vm',
      templateUrl: 'app/shared/components/header/header.view.html',
    });
  
    /** @ngInject */
    function HeaderController() {
  
    }
  
  })();
  