(function() {
  'use strict';

  angular.module('app').component('home', {
    controller: HomeController,
    controllerAs: 'vm',
    templateUrl: 'app/home/home.view.html',
  });

  /** @ngInject */
  function HomeController($log, $rootScope, $translate, SAMPLE_CONSTANT, DataLoader) {
    var vm = this;

    vm.servdata = DataLoader.getData();

    vm.showSampleConstant = function() {
      alert(SAMPLE_CONSTANT);
    }

    vm.switchLanguage = function(language) {
      $translate.use(language);
    }

  }

})();
