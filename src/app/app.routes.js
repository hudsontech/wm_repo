(function() {
  'use strict';

  angular.module('app').config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider.state('home', {
      url: '/home',
      component: 'home',
    });

    $stateProvider.state('about', {
      url: '/about',
      component: 'about',
    });

    $urlRouterProvider.otherwise('/home');
  }

})();
